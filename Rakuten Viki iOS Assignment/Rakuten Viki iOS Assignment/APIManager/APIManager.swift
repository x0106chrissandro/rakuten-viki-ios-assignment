//
//  APIManager.swift
//  Rakuten Viki iOS Assignment
//
//  Created by Chrissandro on 24/2/23.
//

import Foundation

class APIManager: NSObject {
    
    func convertCurrency(from fromCurrency: String, to toCurrency: String, amount: Double, completion: @escaping (Result<Double>) -> Void) {
        
        let urlString = Constants.baseURL
        let url = urlString.appendingPathComponent("pair")
            .appendingPathComponent(fromCurrency)
            .appendingPathComponent(toCurrency)
            .appendingPathComponent("\(amount)")
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.Error(error.localizedDescription))
                return
            }
            guard let data = data else {
                completion(.Error(error?.localizedDescription ?? "There are no new data to show"))
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                guard let result = json["conversion_result"] as? Double else {
                    completion(.Error(error?.localizedDescription ?? "Invalid response from API"))
                    return
                }
                completion(.Success(result))
            } catch let error {
                completion(.Error(error.localizedDescription))
            }
        }.resume()
    }
    
    func getCurrencyCodes(completion: @escaping (Result<[String]>) -> Void) {
        
        let urlString = Constants.baseURL
        let url = urlString.appendingPathComponent("codes")
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.Error(error.localizedDescription))
                return
            }
            guard let data = data else {
                completion(.Error(error?.localizedDescription ?? "There are no new data to show"))
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                guard let result = json["supported_codes"] as? [[String]] else {
                    completion(.Error(error?.localizedDescription ?? "Invalid response from API"))
                    return
                }
                let supportedCodes = result.flatMap( { $0} ).filter( { $0.count <= 3 } )
                completion(.Success(supportedCodes))
            } catch let error {
                completion(.Error(error.localizedDescription))
            }
        }.resume()
    }
    
}

enum Result<T> {
    case Success(T)
    case Error(String)
}

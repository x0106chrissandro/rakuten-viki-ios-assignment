//
//  CurrencyConverterViewController.swift
//  Rakuten Viki iOS Assignment
//
//  Created by Chrissandro on 24/2/23.
//

import UIKit

class CurrencyConverterViewController: UIViewController {
    @IBOutlet var firstCurrencyAmount: UITextField!
    @IBOutlet var secondCurrencyAmount: UITextField!
    @IBOutlet var firstCurrencyCode: UITextField!
    @IBOutlet var secondCurrencyCode: UITextField!
    
    private lazy var firstCurrencyPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        return picker
    }()
    
    private lazy var secondCurrencyPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        return picker
    }()
    
    var supportedCodes: [String]?
    let apiManager: APIManager = APIManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
        // Get supported currency codes from API
        apiManager.getCurrencyCodes() { (result) in
            switch result {
            case .Success(let data):
                self.supportedCodes = data
            case .Error(let message):
                DispatchQueue.main.async {
                    self.showAlertWith(title: "Error", message: message)
                }
            }
        }
        
        firstCurrencyAmount.delegate = self
        secondCurrencyAmount.delegate = self
        
        firstCurrencyAmount.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        secondCurrencyAmount.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }
}

// MARK: - Setup
private extension CurrencyConverterViewController {
    func setupViews() {
        view.backgroundColor = .black
        
        firstCurrencyAmount.attributedPlaceholder = NSAttributedString(
            string: "Amount...",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white]
        )
        secondCurrencyAmount.attributedPlaceholder = NSAttributedString(
            string: "Amount...",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white]
        )
        
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 44))
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonPressed(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        firstCurrencyCode.inputView = firstCurrencyPicker
        firstCurrencyCode.inputAccessoryView = toolBar

        secondCurrencyCode.inputView = secondCurrencyPicker
        secondCurrencyCode.inputAccessoryView = toolBar
    }
    
    func showAlertWith(title: String, message: String, style: UIAlertController.Style = .alert) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        let action = UIAlertAction(title: title, style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Functions
extension CurrencyConverterViewController {
    func clearCurrencyAmount() {
        firstCurrencyAmount.text = nil
        secondCurrencyAmount.text = nil
    }
}

// MARK: - Event handler
extension CurrencyConverterViewController {
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let amount = Double(textField.text ?? ""),
              let firstCurrency = textField == firstCurrencyAmount ? firstCurrencyCode.text : secondCurrencyCode.text,
              let secondCurrency = textField == firstCurrencyAmount ? secondCurrencyCode.text : firstCurrencyCode.text,
              let supportedCodes = supportedCodes else {
            return
        }
        
        // check if both currencies selected are supported
        guard supportedCodes.contains(firstCurrency) && supportedCodes.contains(secondCurrency) else {
            return
        }
        
        apiManager.convertCurrency(from: firstCurrency, to: secondCurrency, amount: amount) { result in
            switch result {
            case .Success(let convertedAmount):
                if textField == self.firstCurrencyAmount {
                    DispatchQueue.main.async {
                        self.secondCurrencyAmount.text = String(convertedAmount)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.firstCurrencyAmount.text = String(convertedAmount)
                    }
                }
            case .Error(let message):
                DispatchQueue.main.async {
                    self.showAlertWith(title: "Error", message: message)
                }
            }
        }
    }
    
    @objc func doneButtonPressed(_ sender: Any) {
        firstCurrencyCode.resignFirstResponder()
        secondCurrencyCode.resignFirstResponder()
    }
}

// MARK: - UIPickerViewDataSource
extension CurrencyConverterViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return supportedCodes?.count ?? 0
    }
}

// MARK: - UIPickerViewDelegate
extension CurrencyConverterViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return supportedCodes?[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        clearCurrencyAmount()
        print("Selected option: \(supportedCodes?[row] ?? "")")
        if firstCurrencyCode.isFirstResponder {
            firstCurrencyCode.text = supportedCodes?[row]
        } else {
            secondCurrencyCode.text = supportedCodes?[row]
        }
    }
}

// MARK: - UITextFieldDelegate
extension CurrencyConverterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet(charactersIn: "1234567890.")
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}

// MARK: - Divider line
extension UIViewController {
    /// Should hide navigation bar divider line
    @objc var isNavigationBarDividerHidden: Bool {
        true
    }
}

//
//  Constants.swift
//  Rakuten Viki iOS Assignment
//
//  Created by Chrissandro on 24/2/23.
//

import Foundation

class Constants {
    public static let apiKey = "36c1083069d38bb408a40d26" // Replace with your own API key
    public static let baseURL = URL(string: "https://v6.exchangerate-api.com/v6/\(apiKey)")!
}

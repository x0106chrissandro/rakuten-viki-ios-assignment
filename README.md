# Rakuten Viki iOS Assignment



## Design pattern

In this assignment I am implementing MVC or Model-View-Controller pattern. I have a view controller 'CurrencyConverterViewController' for all the user interface and business logic. In the APIManager file is where all the API functions are located. Storyboarding is used for the UI with auto-layout constraints.

## Feedback

None, it is a simple assignment of an app and it is reasonable.
